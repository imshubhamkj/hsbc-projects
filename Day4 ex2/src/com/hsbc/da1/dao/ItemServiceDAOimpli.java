package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public class ItemServiceDAOimpli implements ItemServiceDAO {
	
	private static Item[] items = new Item[100];
	private static int counter;

	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		items[counter++] = item;
		return item;
	}

	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		
		for(int index = 0;index < items.length; index++ ) {
			if(items[index].getItemId() == itemId) {
				items[index] = null;
				break;
			}
		}
		
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		// TODO Auto-generated method stub
		for(int index = 0;index < items.length; index++ ) {
			if(items[index].getItemId() == itemId) {
				items[index] = item;
				return item;
			}
		}		
		return null;
	}

	@Override
	public Item[] fetchAllItems() {
		// TODO Auto-generated method stub
		return items;
	}

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		for(int index = 0;index < items.length; index++ ) {
			if(items[index].getItemId() == itemId) {
				return items[index];
			}
		}
		return null;
	}

}
