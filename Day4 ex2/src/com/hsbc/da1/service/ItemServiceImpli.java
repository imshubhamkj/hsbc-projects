package com.hsbc.da1.service;

import com.hsbc.da1.dao.ItemServiceDAO;
import com.hsbc.da1.dao.ItemServiceDAOimpli;
import com.hsbc.da1.model.Item;

public class ItemServiceImpli implements ItemService {
	
	ItemServiceDAO dao = new ItemServiceDAOimpli();

	@Override
	public Item createItem(String itemName, double itemPrice) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemPrice);
		this.dao.saveItem(item);
		
		return item;
	}

	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		this.dao.deleteItem(itemId);
		
	}

	@Override
	public Item[] fetchAllItems() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllItems();
	}

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		
		return this.dao.fetchItemById(itemId);
	}

	@Override
	public double checkPrice(long itemId) {
		// TODO Auto-generated method stub
		
		return this.dao.fetchItemById(itemId).getItemPrice();
	}

	@Override
	public Item updateNameOfItem(long itemId,String itemName) {
		// TODO Auto-generated method stub
		Item item = this.dao.fetchItemById(itemId);
		item.setItemName(itemName);
		Item updatedItem = this.dao.updateItem(itemId, item);
		return updatedItem;
	}

}
