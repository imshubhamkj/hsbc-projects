package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ItemController controller = new ItemController();
		 controller.registerItem("pen", 25.00);

		 controller.registerItem("paper", 30.00);

		Item[] allItems = controller.fetchAllItems();

		for (Item item : allItems) {
			if (item != null) {
				System.out.println("Name of item - " + item.getItemName());
				System.out.println("price of item - " + item.getItemPrice());
			} else {
				break;
			}
		}

	}

}
