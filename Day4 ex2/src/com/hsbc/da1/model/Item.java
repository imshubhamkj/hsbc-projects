package com.hsbc.da1.model;

public class Item {
	
	private static long ITEMIDTRACKER = 100;
	private String itemName;
	private long itemId;
	private double itemPrice;
	
	public Item(String itemName, double itemPrice) {
		this.itemName = itemName;
		this.itemId = ITEMIDTRACKER++;
		this.itemPrice = itemPrice;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public long getItemId() {
		return itemId;
	}

	

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	
	
	

}
