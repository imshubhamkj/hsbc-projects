package com.hsbc.employeeMGT.client;

import java.util.Collection;
import java.util.Iterator;

import com.hsbc.employeeMGT.controller.EmployeeMGTcontroller;
import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.exception.InsufficientLeaves;
import com.hsbc.employeeMGT.exception.LeaveLimitExceed;
import com.hsbc.employeeMGT.model.Employee;

public class EmployeeClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EmployeeMGTcontroller controller = new EmployeeMGTcontroller();
		Employee shubham = controller.registerEmployee("shubham", "comliance");
		Employee shubham2 = controller.registerEmployee("shubham", "risk");
		
		Collection<Employee> employee;
		Iterator<Employee> itr;
		try {
			employee = controller.fetchEmployeesByName("sakshi");
			itr = employee.iterator();
			
			while(itr.hasNext()) {
				System.out.println(itr.next());
			}
			
		} catch (EmployeeNotFound e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());;
		}
		
		
		System.out.println("------------ applying for leave-------------");
		try {
			int leaves = controller.leaveApplication(9, 1001);
			System.out.println(leaves + " leaves granted");
		} catch (InsufficientLeaves e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (LeaveLimitExceed e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (EmployeeNotFound e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("----------leave granted--------------------");
		
		 try {
			employee = controller.fetchEmployeesByName("shubham");
			 itr = employee.iterator();
				
				while(itr.hasNext()) {
					System.out.println(itr.next());
				}

		} catch (EmployeeNotFound e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		 
		
	}

}
