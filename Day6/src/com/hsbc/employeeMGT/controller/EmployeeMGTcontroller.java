package com.hsbc.employeeMGT.controller;

import java.util.Collection;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.exception.InsufficientLeaves;
import com.hsbc.employeeMGT.exception.LeaveLimitExceed;
import com.hsbc.employeeMGT.model.Employee;
import com.hsbc.employeeMGT.service.EmployeeMGTservice;
import com.hsbc.employeeMGT.service.EmployeeMGTserviceImpli;

public class EmployeeMGTcontroller {
	
	EmployeeMGTservice service = new EmployeeMGTserviceImpli();
	
	public Employee registerEmployee(String name,String department) {
		return this.service.createEmployee(name, department);
		
	}
	public void deleteEmployee(long employeeId) {
		this.service.deleteEmployee(employeeId);
	}
	
	public Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound {
		return this.service.fectchEmployeeById(employeeId);
	}
	public Collection<Employee> fetchAllEmployees(){
		return this.service.fetchAllEmployees();
	}
	public Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound{
		return this.service.fetchEmployeesByName(name);
	}
	public int leaveApplication(int leaves, long employeeId) throws InsufficientLeaves, LeaveLimitExceed, EmployeeNotFound {
		return this.service.leaveApplication(leaves, employeeId);
	}

}
