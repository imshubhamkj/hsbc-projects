package com.hsbc.employeeMGT.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.model.Employee;

public class HashSetEmployeeMgtDAOImpli implements EmployeeMGTdao {
	
	private Set<Employee> employees = new HashSet<>(); 
	@Override
	public Employee saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employees.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		long employeeId = employee.getEmployeeId();
		Iterator<Employee> itr = this.employees.iterator();
		Employee updatedEmployee;
		while(itr.hasNext()) {
			Employee nextEmployee = itr.next();
			if(nextEmployee.getEmployeeId() == employeeId) {
				nextEmployee = employee;
				return nextEmployee;
			}
		}
		return null;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee nextEmployee = itr.next();
			if(nextEmployee.getEmployeeId() == employeeId) {
				this.employees.remove(nextEmployee);
				break;
			}
		}
	}

	@Override
	public Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee nextEmployee = itr.next();
			if(nextEmployee.getEmployeeId() == employeeId) {
				return nextEmployee;
			}
		}
		throw new EmployeeNotFound("Employee doesn't exits");
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return new ArrayList<Employee>(employees);
	}

	@Override
	public Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		List<Employee> listOfAllEmployeesOfThisName = new ArrayList<>();
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee nextEmployee = itr.next();
			if(nextEmployee.getName() == name) {
				listOfAllEmployeesOfThisName.add(nextEmployee);
			}
		}
		
		if(!listOfAllEmployeesOfThisName.isEmpty()) {
			return listOfAllEmployeesOfThisName;
		}
		
		
		throw new EmployeeNotFound("Employee wit this name doesn't exist");
	}

}
