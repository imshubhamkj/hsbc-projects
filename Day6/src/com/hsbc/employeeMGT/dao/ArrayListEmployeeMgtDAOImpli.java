package com.hsbc.employeeMGT.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.model.Employee;

public class ArrayListEmployeeMgtDAOImpli implements EmployeeMGTdao {
	
	public List<Employee> employees = new ArrayList<>();
	
	@Override
	public Employee saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employees.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		for(Employee emp : employees) {
			if(emp.getEmployeeId() == employee.getEmployeeId()) {
				emp = employee;
			}
		}
		return null;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		for(Employee emp: employees) {
			if(emp.getEmployeeId() == employeeId) {
				employees.remove(emp);
			}
		}
	}

	@Override
	public Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		for(Employee emp:employees) {
			if(emp.getEmployeeId() == employeeId) {
				return emp;
			}
		}
		throw new EmployeeNotFound("employee doesn't exist");
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return employees;
	}

	@Override
	public Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		List<Employee> listOfEmployeeWithSameName = new ArrayList<>();
		
		for(Employee emp : employees) {
			if(emp.getName().equals(name)) {
				listOfEmployeeWithSameName.add(emp);
			}
		}
		
		if(! listOfEmployeeWithSameName.isEmpty()) {
			return listOfEmployeeWithSameName;
		}
		throw new EmployeeNotFound("Employee with this name doesn't exist");
	}

}
