package com.hsbc.employeeMGT.dao;

import java.util.Collection;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.model.Employee;

public interface EmployeeMGTdao {
	Employee saveEmployee(Employee employee);

	Employee updateEmployee(Employee employee);
	
	void deleteEmployee(long employeeId);
	
	Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound;
	
	Collection<Employee> fetchAllEmployees();

	Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound;

}

