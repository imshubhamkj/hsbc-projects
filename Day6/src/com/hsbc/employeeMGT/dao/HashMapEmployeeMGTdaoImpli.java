package com.hsbc.employeeMGT.dao;

import java.util.*;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.model.Employee;

public class HashMapEmployeeMGTdaoImpli implements EmployeeMGTdao{
	
	Map<Long, Employee> employees = new HashMap<>();

	@Override
	public Employee saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		long employeeId = employee.getEmployeeId();
		employees.put(employeeId,employee);
		return employee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		long employeeId = employee.getEmployeeId();
		employees.put(employeeId,employee);
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		employees.remove(employeeId);
	}

	@Override
	public Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		Employee employee = employees.get(employeeId);
		
		if(employee!=null) {
			return employee;
		}
		
		 throw new EmployeeNotFound("employee does't exist");
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return employees.values();
	}

	@Override
	public Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		List<Employee> listOfEmployeeWithSameName = new ArrayList<>();
		Set<Map.Entry<Long, Employee>> set = employees.entrySet();
		Iterator<Map.Entry<Long, Employee>> itr = set.iterator();
		
		while(itr.hasNext()) {
			Map.Entry<Long, Employee> next = itr.next();
			if(next.getValue().getName().equals(name)) {
				listOfEmployeeWithSameName.add(next.getValue());
			}
		}
		
		if(! listOfEmployeeWithSameName.isEmpty()) {
			return listOfEmployeeWithSameName;
		}
		throw new EmployeeNotFound("employee does't exist");
	}

}
