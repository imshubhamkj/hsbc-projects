package com.hsbc.employeeMGT.service;

import java.util.Collection;

import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.exception.InsufficientLeaves;
import com.hsbc.employeeMGT.exception.LeaveLimitExceed;
import com.hsbc.employeeMGT.model.Employee;

public interface EmployeeMGTservice {
	Employee createEmployee(String name,String department);
	void deleteEmployee(long employeeId);
	Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound;
	Collection<Employee> fetchAllEmployees();
	Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound;
	int leaveApplication(int leaves, long employeeId) throws InsufficientLeaves, LeaveLimitExceed, EmployeeNotFound;
}
