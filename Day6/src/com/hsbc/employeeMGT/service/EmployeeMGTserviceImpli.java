package com.hsbc.employeeMGT.service;

import java.util.Collection;

import com.hsbc.employeeMGT.dao.ArrayListEmployeeMgtDAOImpli;
import com.hsbc.employeeMGT.dao.EmployeeMGTdao;
import com.hsbc.employeeMGT.dao.HashMapEmployeeMGTdaoImpli;
import com.hsbc.employeeMGT.dao.HashSetEmployeeMgtDAOImpli;
import com.hsbc.employeeMGT.exception.EmployeeNotFound;
import com.hsbc.employeeMGT.exception.InsufficientLeaves;
import com.hsbc.employeeMGT.exception.LeaveLimitExceed;
import com.hsbc.employeeMGT.model.Employee;

public class EmployeeMGTserviceImpli implements EmployeeMGTservice {
	
	EmployeeMGTdao dao = new ArrayListEmployeeMgtDAOImpli();

	@Override
	public Employee createEmployee(String name, String department) {
		// TODO Auto-generated method stub
		Employee employee = new Employee(name, department);
		this.dao.saveEmployee(employee);
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployee(employeeId);
		
	}

	@Override
	public Employee fectchEmployeeById(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		return this.dao.fectchEmployeeById(employeeId);
	}

	@Override
	public Collection<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllEmployees();
	}

	@Override
	public Collection<Employee> fetchEmployeesByName(String name) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeesByName(name);
	}


	@Override
	public int leaveApplication(int leaves,long employeeId) throws InsufficientLeaves, LeaveLimitExceed, EmployeeNotFound {
		// TODO Auto-generated method stub
		if(leaves>10) {
			throw new LeaveLimitExceed("you can take maximum 10 leaves at a time");
		}else {
			Employee employee = this.dao.fectchEmployeeById(employeeId);
			int totalRemainingLeaveRequests = employee.getTotalRemainingLeaveRequests();
			
			if(leaves>totalRemainingLeaveRequests) {
				throw new InsufficientLeaves("you don't have sufficient leaves");
			}else {
				totalRemainingLeaveRequests = totalRemainingLeaveRequests - leaves;
				employee.setTotalRemainingLeaveRequests(totalRemainingLeaveRequests);
				this.dao.updateEmployee(employee);
				return leaves;
			}
			
		}
		
	}

	

}
