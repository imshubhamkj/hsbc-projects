package com.hsbc.employeeMGT.model;

import java.util.Objects;

public class Employee {
	private String name;
	private long employeeId;
	private String department;
	private static long counterForId = 1000;
	private int totalRemainingLeaveRequests;

	public Employee(String name, String department) {
		this.name = name;
		this.department = department;
		this.totalRemainingLeaveRequests = 40;
		this.employeeId = counterForId++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getTotalRemainingLeaveRequests() {
		return totalRemainingLeaveRequests;
	}

	public void setTotalRemainingLeaveRequests(int totalRemainingLeaveRequests) {
		this.totalRemainingLeaveRequests = totalRemainingLeaveRequests;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(employeeId, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		return employeeId == other.employeeId && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", employeeId=" + employeeId + ", department=" + department
				+ ", totalRemainingLeaveRequests=" + totalRemainingLeaveRequests + "]";
	}

}
