package com.hsbc.da1.service;

import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.dao.LinkedListBackedSavingsAccountDAOImpli;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountServiceImpli implements SavingsAccountService {
	SavingsAccountDAO dao = new LinkedListBackedSavingsAccountDAOImpli();

	public SavingsAccount createSavingsAccount(String customerName, long accountBalance,String city,String state, long pinCode) {
		Address address = new Address(city, state, pinCode);
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance,address);
		SavingsAccount savingsAccountCreated = this.dao.createSavingsAccount(savingsAccount);
		return savingsAccountCreated;

	}
	
	public SavingsAccount createSavingsAccount(String customerName, long accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount savingsAccountCreated = this.dao.createSavingsAccount(savingsAccount);
		return savingsAccountCreated;

	}

	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);

	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> savingsAccounts = this.dao.fetchSavingsAccounts();
		return savingsAccounts;

	}

	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException {
		return this.dao.fetchSavingsAccountById(accountNumber);

	}
	
	public double withdraw(long accountNumber, double amount) throws InsufficientBalanceException, AccountNotFoundException {
		SavingsAccount savingsAccount =  this.dao.fetchSavingsAccountById(accountNumber);
		
		if(savingsAccount.getAccountBalance() >= amount) {
			double currentBalance = savingsAccount.getAccountBalance()-amount;
			savingsAccount.setAccountBalance(currentBalance);
			
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return amount;
		}
		
		throw new InsufficientBalanceException("Insufficient balance");
		
	}
	
	public double deposite(long accountNumber, double amount) throws AccountNotFoundException {
		SavingsAccount savingsAccount =  this.dao.fetchSavingsAccountById(accountNumber);
		
		if(savingsAccount!=null) {
			double currentBalance = savingsAccount.getAccountBalance()+amount;
			savingsAccount.setAccountBalance(currentBalance);
			
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return amount;
		}
		
		return 0;
	}
	
	public double checkAccountBalance(long accountNumber) throws AccountNotFoundException {
		SavingsAccount savingsAccount =  this.dao.fetchSavingsAccountById(accountNumber);
		
		if(savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		
		return 0;
	}
	
	public double transfer(long senderAccountNumber,long toAccountNumber, double amount) throws InsufficientBalanceException, AccountNotFoundException {
		SavingsAccount sender = this.dao.fetchSavingsAccountById(senderAccountNumber);
		SavingsAccount to = this.dao.fetchSavingsAccountById(toAccountNumber);
		
		if(sender != null && to != null) {
			double updatedBalance = this.withdraw(senderAccountNumber, amount);
			
			if(updatedBalance>0.00) {
				this.deposite(toAccountNumber, amount);
				return amount;
			}
		}
		return 0;
		
		
	}


}
