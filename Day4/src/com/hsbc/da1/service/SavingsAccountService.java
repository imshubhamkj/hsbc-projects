package com.hsbc.da1.service;

import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.LinkedListBackedSavingsAccountDAOImpli;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountService {


	public SavingsAccount createSavingsAccount(String customerName, long accountBalance,String city,String state, long pinCode);	
	public SavingsAccount createSavingsAccount(String customerName, long accountBalance);
	public void deleteSavingsAccount(long accountNumber);
	public List<SavingsAccount> fetchSavingsAccounts();
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException;	
	public double withdraw(long accountNumber, double amount) throws InsufficientBalanceException, AccountNotFoundException;
	public double deposite(long accountNumber, double amount) throws AccountNotFoundException;
	
	public double checkAccountBalance(long accountNumber) throws AccountNotFoundException;
	
	public double transfer(long senderAccountNumber,long toAccountNumber, double amount) throws InsufficientBalanceException, AccountNotFoundException;
}
