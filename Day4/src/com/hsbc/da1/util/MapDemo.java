package com.hsbc.da1.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.hsbc.da1.model.SavingsAccount;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SavingsAccount sa1 = new SavingsAccount("shubham",50_000);
		SavingsAccount sa2 = new SavingsAccount("a",50_00);
		SavingsAccount sa3 = new SavingsAccount("b",55_000);
		SavingsAccount sa4 = new SavingsAccount("c",56_000);
		SavingsAccount sa5 = new SavingsAccount("d",59_000);
		
		Map<SavingsAccount,String> map = new TreeMap<SavingsAccount, String>();
		
		map.put(sa1, sa1.getCustomerName());
		map.put(sa2, sa2.getCustomerName());
		map.put(sa3, sa3.getCustomerName());
		map.put(sa4, sa4.getCustomerName());
		map.put(sa5, sa5.getCustomerName());
		
		Set<Map.Entry<SavingsAccount, String>> entries = map.entrySet();
		
		Iterator<Map.Entry<SavingsAccount, String>> itr = entries.iterator();
		
		while(itr.hasNext()) {
			Map.Entry<SavingsAccount, String> next = itr.next();
			System.out.println(next.getKey()+next.getValue());
		}
		
		
		

	}

}
