package com.hsbc.da1.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import com.hsbc.da1.model.SavingsAccount;

public class TreeSetDemo {
	public static void main(String[] args) {
		
		SavingsAccount sa1 = new SavingsAccount("shubham",50_000);
		SavingsAccount sa2 = new SavingsAccount("a",50_00);
		SavingsAccount sa3 = new SavingsAccount("b",55_000);
		SavingsAccount sa4 = new SavingsAccount("c",56_000);
		SavingsAccount sa5 = new SavingsAccount("d",59_000);
		
		Set<SavingsAccount> set = new TreeSet<>(new sortByNameAsc());
		
		set.add(sa1);
		set.add(sa2);
		set.add(sa3);
		
		Iterator<SavingsAccount> it = set.iterator();
		
		while(it.hasNext()) {
			System.out.println((it.next().toString()));
		}
		
	}

}

class sortByNameAsc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
		// TODO Auto-generated method stub
		return arg0.getCustomerName().compareTo(arg1.getCustomerName());
	}
	
}

class sortByNameDesc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
		// TODO Auto-generated method stub
		return -1 * (arg0.getCustomerName().compareTo(arg1.getCustomerName()));
	}
	
}

//class sortByAccountBalanceAsc implements Comparator<SavingsAccount>{
//
//	@Override
//	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
//		// TODO Auto-generated method stub
//		return (arg0.get);
//	}
//	
//}
