package com.hsbc.da1.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.hsbc.da1.model.SavingsAccount;

public class demoHash {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SavingsAccount sa = new SavingsAccount("shubham", 1000);
		SavingsAccount sa2 = new SavingsAccount("shubham", 1000);
//		sa2.setAccountNumber(1001);
		
		System.out.println((sa == sa2));
		System.out.println(sa.equals(sa2));
		
		Set<SavingsAccount> savingsAccountsSet = new HashSet<SavingsAccount>();
		
		savingsAccountsSet.add(sa);
		savingsAccountsSet.add(sa2);
		
		Iterator<SavingsAccount> iterator = savingsAccountsSet.iterator();
		
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		

	}

}
