package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.LinkedListBackedSavingsAccountDAOImpli;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOfactory {

	private SavingsAccountDAOfactory() {
	}

	public static SavingsAccountDAO getSavingsAccountDAO(int option) {
		if (option == 1) {
			return new ArrayBackedSavingsAccountDAOImpl();
		} else {
			return new LinkedListBackedSavingsAccountDAOImpli();
		}
	}

}
