package com.hsbc.da1.util;

import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpli;

public class SavingsAccountServiceFactory {
	
	public static SavingsAccountService getSavingsAccountService() {
		return new SavingsAccountServiceImpli();
		
	}

}
