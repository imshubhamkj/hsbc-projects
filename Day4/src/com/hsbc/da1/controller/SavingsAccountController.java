package com.hsbc.da1.controller;

import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpli;

public class SavingsAccountController {
	
	
	private SavingsAccountService accountService = new SavingsAccountServiceImpli();
	
	

//	public SavingsAccountController() {
//		// TODO Auto-generated constructor stub
//	}

	public SavingsAccount openSavingsAccount(String customerName, long accountBalance) {

		return this.accountService.createSavingsAccount(customerName, accountBalance);

	}
	
	public SavingsAccount openSavingsAccount(String customerName, long accountBalance,String city,String state,long pinCode) {

		return this.accountService.createSavingsAccount(customerName, accountBalance,city,state,pinCode);

	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.accountService.deleteSavingsAccount(accountNumber);

	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> savingsAccounts = this.accountService.fetchSavingsAccounts();
		return savingsAccounts;

	}

	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException {
		return this.accountService.fetchSavingsAccountById(accountNumber);

	}
	
	public double withdraw(long accountNumber, double amount) throws AccountNotFoundException, InsufficientBalanceException {
		
		
		return this.accountService.withdraw(accountNumber, amount);
	}
	
	public double deposite(long accountNumber, double amount) throws AccountNotFoundException {
		
		return this.accountService.deposite(accountNumber, amount);
	}
	
	public double checkAccountBalance(long accountNumber) throws AccountNotFoundException {
		
		
		return this.accountService.checkAccountBalance(accountNumber);
	}
	
	public double transfer(long senderAccountNumber,long toAccountNumber, double amount) throws AccountNotFoundException, InsufficientBalanceException {
		return this.accountService.transfer(senderAccountNumber, toAccountNumber, amount);
	}


}
