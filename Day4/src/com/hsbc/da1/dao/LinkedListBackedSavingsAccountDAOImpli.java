package com.hsbc.da1.dao;

import java.util.LinkedList;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class LinkedListBackedSavingsAccountDAOImpli implements SavingsAccountDAO {
	List<SavingsAccount> savingsAccounts = new LinkedList<>();
	
	

	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		savingsAccounts.add(savingsAccount);
		
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				
				acc = savingsAccount;
				
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				savingsAccounts.remove(acc);
			}
		}
		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccounts;
	}

	@Override
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException {
		// TODO Auto-generated method stub
		for(SavingsAccount acc : savingsAccounts) {
			if(acc.getAccountNumber() == accountNumber) {
				return acc;
			}
		}
		throw new AccountNotFoundException("Account doesn't exist");
	}
	
	

}
