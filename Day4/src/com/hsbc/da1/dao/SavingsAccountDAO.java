package com.hsbc.da1.dao;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;
import java.util.List;

public interface SavingsAccountDAO {
		
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);
	
	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetchSavingsAccounts();
	
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException;
}