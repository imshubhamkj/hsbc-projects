package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter;

	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {

		for (int index = 0; index < savingsAccounts.length; index++) {
			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				return savingsAccount;
			}
		}
		return null;
	}

	public void deleteSavingsAccount(long accountNumber) {

		for (int index = 0; index < savingsAccounts.length; index++) {
			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}

	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return Arrays.asList(savingsAccounts);

	}

	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws AccountNotFoundException {

		for (int index = 0; index < savingsAccounts.length; index++) {
			if (savingsAccounts[index] != null) {
				long expectedAccountNumber = savingsAccounts[index].getAccountNumber();
				if (expectedAccountNumber == accountNumber) {
					return savingsAccounts[index];

				}
			}
		}
		throw new AccountNotFoundException("Account not found");

	}

}
