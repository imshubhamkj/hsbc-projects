package com.hsbc.da1.client;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountClient {

	public static void main(String[] args) {
	
		
		SavingsAccountController controller = new SavingsAccountController();
		
		SavingsAccount shubhamSavingsAccount = controller.openSavingsAccount("shubham", 50_000);  
		SavingsAccount shantanuSavingsAccount = controller.openSavingsAccount("shantanu", 20_000);
		SavingsAccount shivamSavingsAccount = controller.openSavingsAccount("shivam", 40_000,"purnea","Bihar",854301);
		
		
		
		System.out.println("AccountId: "+ shubhamSavingsAccount.getAccountNumber());
		System.out.println("Account Balance: "+ shubhamSavingsAccount.getCustomerName());
		System.out.println("Account Balance: "+ shubhamSavingsAccount.getAccountBalance());
		
		System.out.println("AccountId: "+ shantanuSavingsAccount.getAccountNumber());
		System.out.println("Account Balance: "+ shantanuSavingsAccount.getAccountBalance());
		System.out.println("Account Balance: "+ shantanuSavingsAccount.getCustomerName());
		
		
		System.out.println("--------------Transferring----------------");
		try {
		double transferredAmount = controller.transfer(1001, 1002, 10_000);
		if(transferredAmount > 0) {
			System.out.println("Transferred Amount "+transferredAmount);
		}
		}catch(AccountNotFoundException e) {
			System.out.println(e.getMessage());
		}catch(InsufficientBalanceException e) {
			System.out.println(e.getMessage());
		}
		finally {
		System.out.println("--------------Transferred----------------");
		
		System.out.println("AccountId: "+ shubhamSavingsAccount.getAccountNumber());
		System.out.println("Account Balance: "+ shubhamSavingsAccount.getCustomerName());
		System.out.println("Account Balance: "+ shubhamSavingsAccount.getAccountBalance());
		
		System.out.println("AccountId: "+ shantanuSavingsAccount.getAccountNumber());
		System.out.println("Account Balance: "+ shantanuSavingsAccount.getAccountBalance());
		System.out.println("Account Balance: "+ shantanuSavingsAccount.getCustomerName());
		

		}
		

	}

}
