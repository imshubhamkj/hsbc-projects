package com.hsbc.da1.model;

import java.util.Objects;

public class SavingsAccount implements Comparable<SavingsAccount> {
	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private static long counter =1000;
	
	private Address address;
	

	public SavingsAccount(String customerName,double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(accountNumber, customerName);
	}
	
	

	@Override
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", address=" + address + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SavingsAccount))
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		return accountNumber == other.accountNumber && Objects.equals(customerName, other.customerName);
	}

	public SavingsAccount(String customerName,double accountBalance,Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.address = address;
		this.accountNumber = ++counter;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	@Override
	public int compareTo(SavingsAccount secondSavingsAccount) {
		// TODO Auto-generated method stub
		return this.getCustomerName().compareTo(secondSavingsAccount.getCustomerName());
	}


}
