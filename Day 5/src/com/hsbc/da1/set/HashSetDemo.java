package com.hsbc.da1.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	public static void main(String args[]) {
		Set<Integer> name = new HashSet<Integer>();
		
		name.add(20);

		name.add(20);
		name.add(20);
		name.add(20);
		name.add(20);
		name.add(20);
		name.add(25);
		System.out.println("size"+ name.size());
		System.out.println(name.contains(25));
		
		Iterator<Integer> it = name.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}

}
