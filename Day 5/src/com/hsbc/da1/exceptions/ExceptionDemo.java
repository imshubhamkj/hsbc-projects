package com.hsbc.da1.exceptions;

public class ExceptionDemo {
	public static void main(String args[]) {
		
		int a = 20;
		int b = 0;
		
		
		int[] arr = {1,2,3,4};
		
		try {
			System.out.println("a divided by b = " + (b/a));
			
			System.out.println("last element of array "+ arr[arr.length]);
		}
		catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {
			if( e instanceof ArithmeticException) {
			System.out.println("Numbers can't be divided by 0");
			}else {
				System.out.println("Index should be less than " + arr.length);
			}
		}catch(Exception e ) {
			System.out.println("Generic exception");;
		}
		
		finally {
			System.out.println("this will be executed anyway");
		}
		
		
	}

}
