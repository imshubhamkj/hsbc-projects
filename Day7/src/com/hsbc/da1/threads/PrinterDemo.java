package com.hsbc.da1.threads;

public class PrinterDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Printer printer = new Printer();
		Runnable job1 = new Job(printer, 10, 30);

		Runnable job2 = new Job(printer, 50, 60);

		Runnable job3 = new Job(printer, 40, 50);
		
		Thread t1 = new Thread(job1,"JOB1");
		Thread t2 = new Thread(job2,"JOB2");
		Thread t3 = new Thread(job3,"JOB3");
		
		t1.start();
		t2.start();
		t3.start();
		
		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("COMPLETED ALL TASK");
		
		
		

	}

}

class Printer{
	public synchronized void print(int start,int end) throws InterruptedException {
		if(start<end) {
			System.out.println("printing " + start + "page");
			
			for(int i=start ;i<=end; i++) {
				System.out.println("printing page" + i);
				Thread.sleep(2000);
			}
			System.out.println("printing " + end + "page");
			
		}
	}
}


class Job implements Runnable{
	private Printer printer;
	private int start;
	private int end;
	

	public Job(Printer printer,int start,int end) {
		this.printer = printer;
		this.start = start;
		this.end = end;
	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			System.out.println("job " + Thread.currentThread().getName() + "started");
			printer.print(start, end);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
