package com.hsbc.da1.threads;

public class ChildThread extends Thread {
	
	public static void main(String[] args) {
		
		
		ChildThread t1 = new ChildThread("Thread1");
		t1.start();

		ChildThread t2 = new ChildThread("Thread2");
		t2.start();
		
		for (int i = 0; i < 10; i++) {
			System.out.println("into the " + Thread.currentThread().getName() + " Thread");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());;
			}
			
		}
		
	}
	
	public ChildThread(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public ChildThread() {
	}
	
	public void run() {
		
		System.out.println("===============" + Thread.currentThread().getName() + "======================");
		
		for (int i = 0; i < 10; i++) {
			System.out.println("Inside the " + Thread.currentThread().getName());
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());;
			}
			
		}
		
	}

}
