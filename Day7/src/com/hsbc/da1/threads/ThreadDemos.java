package com.hsbc.da1.threads;

public class ThreadDemos extends Thread {
	
	public ThreadDemos(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public ThreadDemos() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		
	
		ThreadDemos t = new ThreadDemos("Child class");
		t.start();
		
		
		Runnable threadGooglePhotos = new GooglePhotos();
		Runnable threadFliker = new Flicker();
		Runnable threadPicassa = new Picassa();
		
		Thread t1 = new Thread(threadGooglePhotos,"Googlephoto thread");
		Thread t2 = new Thread(threadFliker,"Flicker thread");
		Thread t3 = new Thread(threadPicassa,"picassa thread");
		t1.start();
		t2.start();
		t3.start();
		
		try {
			t1.join();
			t2.join();
			
			t3.join();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("------ exiting---" + Thread.currentThread().getName() + "---Thread-----------------");
		
		
		
	}
	 
	
	public void run() {
		System.out.println("------ inside---" + Thread.currentThread().getName() + "---Thread-----------------");
		
		try {
			Thread.sleep(2000);
			System.out.println("wiated 2000ms inside " + Thread.currentThread().getName() + " Thread");
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());;
		}
		System.out.println("------ exiting---" + Thread.currentThread().getName() + "---Thread-----------------");
	}

}

class GooglePhotos implements Runnable{
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("------ inside---" + Thread.currentThread().getName() + "---Thread-----------------");
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("------ outside---" + Thread.currentThread().getName() + "---Thread-----------------");
	}
	
}

class Flicker implements Runnable{
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("------ inside---" + Thread.currentThread().getName() + "---Thread-----------------");
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("------ outside---" + Thread.currentThread().getName() + "---Thread-----------------");
	}
	
}

class Picassa implements Runnable{
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("------ inside---" + Thread.currentThread().getName() + "---Thread-----------------");
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("------ outside---" + Thread.currentThread().getName() + "---Thread-----------------");
	}
	
}
