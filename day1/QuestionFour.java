public class QuestionFour{
    /**
    *Question 4
     */
    public static void main(String args[]){
    
    int[] numbersArray = new int[5];

    for( int index = 0;index < args.length;index++){
        numbersArray[index] = Integer.parseInt( args[index] );
    }
    int minimumNumber = Integer.MAX_VALUE;
    int maximumNumber = Integer.MIN_VALUE;

    for(int index = 0;index < numbersArray.length;index++){
        if(minimumNumber > numbersArray[index]){
            minimumNumber = numbersArray[index];
        }
        if(maximumNumber < numbersArray[index]){
            maximumNumber = numbersArray[index];
        }
    }
    System.out.println("Minimum number  "+minimumNumber);
    
    System.out.println("Maximum number  "+maximumNumber);
    }
}