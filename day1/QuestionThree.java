public class QuestionThree{
    /**
    *Question 3
     */
    
    public static void main(String args[]){
        float totalBill= Float.parseFloat(args[0]);
        String StateCode = args[1];
        float tax;

        switch( StateCode ){
            case "KA":
            tax = (totalBill * 15)/100;
            break;
            case "TN":
            tax = (totalBill * 18)/100;
            break;
            case "MH":
            tax = (totalBill * 20)/100;
            break;
            default:
            tax = (totalBill * 12)/100;
        }


        System.out.println(totalBill+tax);

        
    }
}