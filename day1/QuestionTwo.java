public class QuestionTwo{
    /**
    *Question 2
     */
    public static void main(String args[]){
        String letter = args[0];
        String vowel = "aeiou";
        if(vowel.contains(letter)){
            System.out.println("Vowel");
        }else{
            System.out.println("Consonant");
        }

    }
}