public class TwoDim{
    /**
    *TwoDimension examples
     */
    public static void main(String args[]){
       
       int row = 5;
       int col = 5;

       int[][] arr = new int[row][col];
       

       populate(arr);
       print(arr);


        }

    public static int[][] populate(int[][] arr){
        int firstElement = 10;
        for(int rowIndex = 0; rowIndex < 5; rowIndex++){
            for(int colIndex = 0; colIndex<5;colIndex++){
                arr[rowIndex][colIndex] = firstElement++;
                // System.out.println(arr[rowIndex][colIndex]);
            }
        }
        return arr;
    }

    public static void print(int[][] arr){
        for(int rowIndex = 0; rowIndex < 5; rowIndex++){
            for(int colIndex = 0; colIndex<5;colIndex++){
                System.out.print(arr[rowIndex][colIndex]+" ");
            }
            System.out.println();
        }
    }
    
}