public class QuestionOne{
    /**
    *Question 1
     */
    public static void main(String args[]){
        String enterDay = args[0];

        String WhichDay = (enterDay.equals("SUNDAY") || enterDay.equals("SATURDAY"))? "WEEKEND" : "WEEKDAY";
        
        System.out.println(WhichDay);
    }
}