public class CurrentAccountBluePrint {
    private static long accountNumberTracker = 10000;
    private long accountNumber;
    private double accountBalance;
    private String GSTnumber;
    private String name;
    private String bussinessName;
    // private String communicationAddress;

    private Address address;
    
    public CurrentAccountBluePrint(String name,String gstNumber,String bussinessName,String city,String pin,String state){
        this.name = name;
        this.GSTnumber = gstNumber;
        this.bussinessName = bussinessName;
        this.address = new Address(city,pin,state);
        this.accountBalance = 50_000;
        this.accountNumber = ++ accountNumberTracker;
    }

    public CurrentAccountBluePrint(String name,String gstNumber,String bussinessName,Address address){
        this.address = address;
        this.name = name;
        this.GSTnumber = gstNumber;
        this.bussinessName = bussinessName;
        this.accountBalance = 50_000;

    }
    
    public void withdrawal(double amount){
        if(this.accountBalance - 50_000 > amount){
            this.accountBalance = this.accountBalance - amount;
            System.out.println("withdrawal successfull");
            System.out.println("remaining amount "+ this.accountBalance);
        }else{
            System.out.println("Insufficient balance - withdrawal failed");
        }
    }
    
    public void deposite(double amount){
        this.accountBalance = this.accountBalance + amount;
        System.out.println("Deposite done");  
        System.out.println("toll " + this.accountBalance );
    }
    
    public void updateAddress(Address address){
        address.changeAddress(address);
    }
    public void showAddress(){
        address.printAddress();
    }

    public void accountBalanceInfo(){
        System.out.println(this.name+this.accountBalance);
    }
    public void transferMoney(double amount,CurrentAccountBluePrint reciver){

        if(this.accountBalance - 50000 >= amount){
            this.withdrawal(amount);
            reciver.deposite(amount);
            System.out.println("Amount transfered");
        }else{
            System.out.println("insufficent balance");
        }

    }
    
}