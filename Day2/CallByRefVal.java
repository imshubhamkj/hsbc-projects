class CallByRefVal{
    public static void main(String args[]){
        int[] arr =new int[]{1,2,3};

        int firstNumber = 10;
        int secondNumber = 20;
        System.out.println("------------------------------");
        System.out.println("call by value");
        System.out.println(" status before calling " + firstNumber +" " + secondNumber);
        
        callByValue(firstNumber,secondNumber);
        System.out.println(" status after calling " + firstNumber +" " + secondNumber);
        
        System.out.println("------------------------------");
        System.out.println("call by ref");
        System.out.println(" status before calling ");
        for(int i:arr){
            System.out.println(i);
        }

        callByRef(arr);

        System.out.println(" status after calling ");
        for(int i:arr){
            System.out.println(i);
        }

    }

    public static void callByValue(int first,int second){
        first = 20 + first;
        second = 30 + second;
        System.out.println("changes inside the function "+first + " "+ second);

    }

    public static void callByRef(int[] arr){
        arr[0] = 0;
        System.out.println("changes inside the function");
        for(int i:arr){
            System.out.println(i);
        }
    }
}