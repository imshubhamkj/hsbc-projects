class Address{
    private String city;

    private String pinCode;
    
    private String state;

    public Address(String city,String pinCode, String state){
        this.city = city;
        this.pinCode = pinCode;
        this.state = state;
    }

    public void changeAddress(Address address){
        this.city = address.city;
        this.pinCode = address.pinCode;
        this.state = address.state;
    }

    public void printAddress(){
        System.out.println(this.city + this.state + this.pinCode);
    }
    
}