public class InsuranceClient{
    public static void main(String[] args) {

        Insurance insurance = null;

        if(Integer.parseInt(args[0]) == 1){
            System.out.println("Tata chosen");
            insurance = new TataAIG();
        }else{
            System.out.println("bajaj chosen");
            insurance = new BajajInsurance();
        }

        double premiumAmount = insurance.calculatePremium("car","xyzzz",5000.00);
        String policyNumber = insurance.payPremium(2000.00,"br12000","xyzzz");

        System.out.println("premium amount " + premiumAmount);
        System.out.println("policy number " +policyNumber);

    }
}

interface Insurance{
    double calculatePremium(String vehicleName,String model,double insuredAmount);
    String payPremium(double premiumAmount,String vehicleNumber,String model);

}

class BajajInsurance implements Insurance{
    public static long policyNumberTracker = 100;
    public long policyNumber;

    public BajajInsurance(){
        policyNumber = ++ policyNumberTracker;
    }

    public double calculatePremium(String vehicleName,String model,double insuredAmount){
        System.out.println("premium for BajaInsurance will be "+ 2000 + " for the provided details");
        return 2000.00;
    }
    public String payPremium(double premiumAmount,String vehicleNumber,String model){
        System.out.println("premium payed for policyNumber " + (policyNumber));
        return policyNumber+"";
    }

}


class TataAIG implements Insurance{
    public static long policyNumberTracker = 1000;
    public long policyNumber;

    public TataAIG(){
        policyNumber = ++ policyNumberTracker;
    }
    public double calculatePremium(String vehicleName,String model,double insuredAmount){
        System.out.println("premium for TataAIG will be "+ 3000.00 + " for the provided details");
        return 2000.00;
    }
    public String payPremium(double premiumAmount,String vehicleNumber,String model){
        System.out.println("premium payed for policyNumber " + (policyNumber));
        return policyNumber+"";
    }

}