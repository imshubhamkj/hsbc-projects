public class PaymentGatewayClient{
    public static void main(String[] args) {
        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;

        if(args[0].equals("1")){
            GooglePay gpay = new GooglePay();
            paymentGateway = gpay;
            mobileRecharge = gpay;
        }else if(args[0].equals("2")){
            PhonePay pPay = new PhonePay();
            paymentGateway = pPay;
            mobileRecharge = pPay;
        }else{
            JioPay jPay = new JioPay();
            paymentGateway = jPay;
        }

        paymentGateway.pay("Shubham","Shivam",15_000,"confirm");
        if(mobileRecharge != null){
            mobileRecharge.recharge(123456,200);
        }
    }
}

interface PaymentGateway{
    void pay(String from,String to,double amount,String note);
}

interface MobileRecharge{
    void recharge(long number,double amount);
}

class GooglePay implements PaymentGateway,MobileRecharge{
    public void pay(String from,String to,double amount,String note) {
        System.out.println(from + " payed " + amount + " to " + to + " with a message " +note);
        
    }

    public void recharge(long number,double amount){
        System.out.println("payment for "+amount+" recieved through Gpay");
    }
}

class PhonePay implements PaymentGateway,MobileRecharge{
    public void pay(String from,String to,double amount,String note) {
        System.out.println(from + " payed " + amount + " to " + to + " with a message " +note);
        
    }

    public void recharge(long number,double amount){
        System.out.println("payment for "+amount+" recieved through phonepay");
    }
}

class JioPay implements PaymentGateway{
    public void pay(String from,String to,double amount,String note) {
        System.out.println(from + " payed " + amount + " to " + to + " with a message" +note);
        
    }

}