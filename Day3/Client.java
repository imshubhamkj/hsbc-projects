abstract class BankAccount{
    public static long accountNumberTracker = 10_000;
    private String name;
    private long accountNumber;
    private Address address;
    private double accountBalance;

    public BankAccount(String name,Address address,double accountBalance){
        this.name = name;
        this.adress = address;
        this.accountBalance = accountBalance;
        this.accountNumber = ++ accountNumberTracker;
    }

    public abstract void withdraw();

    public final void deposit(double amount){
        if(amount>0){
            this.accountBalance = this.accountBalance + amount;
        }
    }
    public abstract boolean checkMinBalanceEligiblity();

    public abstract boolean loanEligiblity(double loanAmount);
    
    public double checkBalance(){
        return this.accountBalance;
    }

    public void updateAccountBalance(double balance){
        this.accountBalance = balance;
    }
} 

final class CurrentAccount extends BankAccount{
    private String businessName;
    private String gstNumber;
    private double maxLoanAmount = 25_00_000.00;
    
    public CurrentAccount(String name,Address address,String businessName,String gstNumber){
        super(name,address,25_000);
        this.businessName = businessName;
        this.gstNumber = gstNumber;
    }

    public final void withdraw(double amount){
        boolean flag  = checkMinBalanceEligiblity(double amount);

        if(flag){
            System.out.println("withdrawing " + amount);
            updateAccountBalance(checkBalance() - amount);

            System.out.println("Account balance " + checkBalance());

        }
    }

    public boolean checkMinBalanceEligiblity() {
        if(checkBalance() >= 25_000){
            return true;
        }

        return false;
        
    }

    public boolean checkMinBalanceEligiblity(double amount) {
        if(checkBalance()-amount >= 25_000){
            return true;
        }

        return false;
        
    }

    public void loanEligiblity(double loanAmount){
        if(loanAmount>this.maxLoanAmount){
            System.out.pirntln("Loan can't be granted");
        }else{
            super.deposite(loanAmount);
            System.out.pirntln("Loan granted");

        }
    }

}



final class SavingsAccount extends BankAccount{
    private double maxLoanAmount =5_00_000.00;

    public SavingsAccount(String name,Address address){
        super(name,address,10_000);
    }

    public void loanEligiblity(double loanAmount){
        if(loanAmount>this.maxLoanAmount){
            System.out.pirntln("Loan can't be granted");
        }else{
            super.deposite(loanAmount);
            System.out.pirntln("Loan granted");

        }
    }

    public boolean checkMinBalanceEligiblity() {
        if(checkBalance() >= 10_000){
            return true;
        }

        return false;
        
    }

    public boolean checkMinBalanceEligiblity(double amount) {
        if(checkBalance()-amount >= 10_000){
            return true;
        }

        return false;
        
    }

    public final void withdraw(double amount){
        boolean flag  = checkMinBalanceEligiblity(double amount);

        if(flag){
            System.out.println("withdrawing " + amount);
            updateAccountBalance(checkBalance() - amount);

            System.out.println("Account balance " + checkBalance());

        }
    }

}

final class SalariedAccount extends BankAccount{

    public SalariedAccount(String name,Address address){
        super(name,address,0);
    }

    public final void withdraw(double amount){
        

        if(super.checkBalance){
            System.out.println("withdrawing " + amount);
            updateAccountBalance(checkBalance() - amount);

            System.out.println("Account balance " + checkBalance());

        }
    }



}

class Address{
    private String city;

    private String pinCode;
    
    private String state;

    public Address(String city,String pinCode, String state){
        this.city = city;
        this.pinCode = pinCode;
        this.state = state;
    }

    public void changeAddress(Address address){
        this.city = address.city;
        this.pinCode = address.pinCode;
        this.state = address.state;
    }

    public void printAddress(){
        System.out.println(this.city + this.state + this.pinCode);
    }
    
}















public class Client{
    public static void main(String[] args) {
        
    }
}